# -*- coding: utf-8 -*-
from copy import copy
import json

import attr

from .constants import Labels, Units, Defaults
from .exceptions import InvalidSenMLDocument, InvalidSenMLMeasurement


class RecordEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Measurement):
            return [obj.real, obj.imag]
        # Let the base class default method raise the TypeError
        return super().default(self, obj)


class Pack:
    """A senML measurement pack. It is comprised of a list of measurements.

    Can be used to either parse a senML document or create a senML document
    """

    def __init__(self, *args, **kwargs):
        self.measurements = []
        self.current_base = MeasurementBase()

    @classmethod
    def from_json(cls, json_doc):
        """Create a senML pack from a json document"""
        if not isinstance(json_doc, list):
            raise InvalidSenMLDocument('The json document should have a top level array')

        pack = Pack()
        pack.parse_json(json_doc)

        return pack

    def update_base(self, measurement_doc):
        """Update the base info for the rest of the pack

        Args:
            measurement_doc (dict): A dictionary with the parsed json document for the
                current measurement
        Returns:
            bool: True if there was a change
        """
        new_base = MeasurementBase.from_json(measurement_doc, self.current_base)

        if new_base != self.current_base:
            self.current_base = new_base
            return True

        return False

    def parse_json(self, json_doc):
        for record in json_doc:
            self.update_base(record)
            # TODO : use the change info and emit an event...
            self.measurements.append(Measurement.from_json(record, self.current_base))

    def resolve(self):
        for measurement in self.measurements:
            measurement.resolve()

    def __iter__(self):
        return (meas for meas in self.measurements)


@attr.s
class MeasurementBase:
    name = attr.ib(default='')
    time = attr.ib(default=Defaults.DEFAULTTIME)
    value = attr.ib(
        default=None,
        validator=attr.validators.optional(
            attr.validators.instance_of((int, float))
        )
    )
    sum_ = attr.ib(
        default=None,
        validator=attr.validators.optional(
            attr.validators.instance_of((int, float))
        )
    )
    unit = attr.ib(default='')
    version = attr.ib(default=Defaults.DEFAULTVERSION)

    @classmethod
    def from_json(cls, json_doc, old_base=None):
        """Creates a new MeasurementBase object from a parsed json document (a dictionary)"""

        # If we got no old base then create an empty object with the default values
        # we cannot do that in the function arguments because it is a classmethod
        if old_base is None:
            old_base = cls()

        # First we need to translate the json labels to the Measurement
        # attribute names
        translated_doc = {
            'name': json_doc.get(Labels.BASENAME, old_base.name),
            'time': json_doc.get(Labels.BASETIME, old_base.time),
            'value': json_doc.get(Labels.BASEVALUE, old_base.value),
            'sum_': json_doc.get(Labels.BASESUM, old_base.sum_),
            'unit': json_doc.get(Labels.BASEUNIT,  old_base.unit),
            'version': json_doc.get(Labels.BASEVERSION, old_base.version),
        }

        # Then we create the object (with the help of attr)
        return cls(**translated_doc)


@attr.s
class Measurement:
    string_value = attr.ib(
        default=None,
        validator=attr.validators.optional(
            attr.validators.instance_of(str)
        )
    )
    boolean_value = attr.ib(
        default=None,
        validator=attr.validators.optional(
            attr.validators.instance_of(bool)
        )
    )
    data_value = attr.ib(
        default=None,
        validator=attr.validators.optional(
            attr.validators.instance_of(str)
        )
    )
    base = attr.ib(default=None)
    update_time = attr.ib(default=None)
    value = attr.ib(
        default=None,
        validator=attr.validators.optional(
            attr.validators.instance_of((int, float))
        )
    )
    name = attr.ib(default='')
    version = attr.ib(default=Defaults.DEFAULTVERSION)
    time = attr.ib(default=Defaults.DEFAULTTIME)
    unit = attr.ib(default='')
    sum_ = attr.ib(
        default=None,
        validator=attr.validators.optional(
            attr.validators.instance_of((int, float))
        )
    )

    def get_value(self):
        """Gets any value that is present in this measurement"""
        if self.value is not None:
            return self.value
        elif self.boolean_value is not None:
            return self.boolean_value
        elif self.string_value is not None:
            return self.string_value
        elif self.data_value is not None:
            return self.data_value
        else:
            return None

    @classmethod
    def from_json(cls, json_doc, base=MeasurementBase()):
        """Creates a new Measurement object from a parsed json document (a dictionary)"""

        # First we need to translate the json labels to the Measurement
        # attribute names
        translated_doc = {
            'name': json_doc.get(Labels.NAME, ''),
            'time': json_doc.get(Labels.TIME, None),
            'unit': json_doc.get(Labels.UNIT,  ''),
            'update_time': json_doc.get(Labels.UPDATETIME, None),
            'value': json_doc.get(Labels.VALUE, None),
            'string_value': json_doc.get(Labels.STRINGVALUE, None),
            'boolean_value': json_doc.get(Labels.BOOLEANVALUE, None),
            'data_value': json_doc.get(Labels.DATAVALUE, None),
            'sum_': json_doc.get(Labels.SUM, None)
        }

        # Then we create the object (with the help of attr)
        return cls(**translated_doc, base=base)

    @sum_.validator
    def allow_no_values_with_sum(self, attribute, value):
        if value is None and \
                self.value is None and \
                self.boolean_value is None and \
                self.string_value is None and \
                self.data_value is None:
            raise InvalidSenMLMeasurement('Needs at least one type of '
                                          'measurement or a sum')

    @value.validator
    def only_allow_one_value_type(self, attribute, value):
        value_count = 0
        if value is not None:
            value_count += 1
        if self.string_value is not None:
            value_count += 1
        if self.boolean_value is not None:
            value_count += 1
        if self.data_value is not None:
            value_count += 1

        #  Check if we have more than one value type and raise an error
        # no values is handled elsewhere
        if value_count > 1:
            raise InvalidSenMLMeasurement("Only one type of values is allowed")

    def resolve(self):
        """Update the measurement values using the base values"""

        # Name is never none... it is the empty string in the worst case
        self.name = self.base.name + self.name
        # Time is never none... it is 0 in the worst case
        self.time = self.base.time + self.time
        # Unit is never none... it is the empty string in the worst case
        self.unit = self.base.unit

        # If base value is not none than add to the local value else leave as it is
        if self.base.value is not None:
            self.value = self.base.value if self.value is None else self.base.value + self.value

        # If base sum is not none than add to the local sum else leave as it is
        if self.base.sum_ is not None:
            self.sum_ = self.base.sum_ if self.sum_ is None else self.base.sum_ + self.sum_
