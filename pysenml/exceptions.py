class PysenMLException(Exception):
    """Base exception for the pysenml package"""

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "{} : {}".format(self.__class__, self.message)


class InvalidSenMLMeasurement(PysenMLException):
    """Invalid measurement"""
    pass


class InvalidSenMLDocument(PysenMLException):
    """Invalid senml document"""
    pass
