=======
pysenml
=======


.. image:: https://img.shields.io/pypi/v/pysenml.svg
        :target: https://pypi.python.org/pypi/pysenml

.. image:: https://img.shields.io/travis/tasosval/pysenml.svg
        :target: https://travis-ci.org/tasosval/pysenml

.. image:: https://readthedocs.org/projects/pysenml/badge/?version=latest
        :target: https://pysenml.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




A python library for working with RFC 8428 SenML formats


* Free software: MIT license
* Documentation: https://pysenml.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
