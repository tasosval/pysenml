==========================
fixtures from the RFC spec
==========================

Note that the ex9.json was originally:

    [
        {"bn":"urn:dev:ow:10e2073a01080063:"},
        {"n":"temp","u":"Cel","v":23.1},
        {"n":"heat","u":"/","v":1},
        {"n":"fan","u":"/","v":0}
    ]

but this does not create a valid result. The first measurement would be 0 with no name...

I updated it to the current document.
