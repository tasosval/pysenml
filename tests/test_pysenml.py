#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `pysenml` package."""
import json

import pytest

import pysenml
from pysenml import InvalidSenMLDocument

FIXTURES_PATH = 'tests\\fixtures\\ex{}.json'


class TestMeasurementBase:

    def test_equality(self):
        mb1 = pysenml.MeasurementBase(value=4)
        mb2 = pysenml.MeasurementBase(value=4)

        assert mb1 == mb2
        assert not mb1 != mb2
        assert id(mb1) != id(mb2)

    def test_default_object_creation(self):
        mb1 = pysenml.MeasurementBase()
        mb2 = pysenml.MeasurementBase.from_json({})

        assert mb1 == mb2

    def test_object_creation(self):
        mb1 = pysenml.MeasurementBase(
            name='test',
            time=21,
            value=4,
            sum_=8,
            unit=pysenml.Units.AMPERE,
            version=12

        )
        mb2 = pysenml.MeasurementBase.from_json({
            pysenml.Labels.BASENAME: 'test',
            pysenml.Labels.BASETIME: 21,
            pysenml.Labels.BASEVALUE: 4,
            pysenml.Labels.BASESUM: 8,
            pysenml.Labels.BASEUNIT: pysenml.Units.AMPERE,
            pysenml.Labels.BASEVERSION: 12
        })
        assert mb1 == mb2


class TestMeasurement:

    def test_normal_values(self):
        test_doc = {
            'value': 0
        }

        pysenml.Measurement(**test_doc)

    def test_multiple_values_throws_error(self):
        test_doc = {
            'value': 0,
            'string_value': 'test'
        }

        with pytest.raises(pysenml.InvalidSenMLMeasurement):
            pysenml.Measurement(**test_doc)

    def test_at_least_one_value(self):
        test_doc = {
            'time': 0
        }

        with pytest.raises(pysenml.InvalidSenMLMeasurement) as ex:
            pysenml.Measurement(**test_doc)
        assert 'Needs at least one type' in ex.value.message

    def test_from_json(self):
        test_doc = {'v': 0}

        measurement = pysenml.Measurement.from_json(test_doc)

        assert measurement.value == 0


class TestPack:

    def test_one_measurement(self):
        test_pack = [{
            'v': 0
        }]

        pack = pysenml.Pack.from_json(test_pack)

        assert len(pack.measurements) == 1
        assert pack.measurements[0].value == 0

    def test_needs_valid_json_file(self):
        test_pack = {
            'v': 1
        }

        with pytest.raises(InvalidSenMLDocument):
            pysenml.Pack.from_json(test_pack)

    def test_two_measurements(self):
        test_pack = [{
            'v': 0
        }, {
            'v': 1,
            't': 1
        }]

        pack = pysenml.Pack.from_json(test_pack)

        assert len(pack.measurements) == 2
        assert pack.measurements[0].value == 0
        assert pack.measurements[1].value == 1
        assert pack.measurements[1].time == 1

        assert id(pack.measurements[0].base) == id(pack.measurements[1].base)

    def test_two_bases(self):
        with open(FIXTURES_PATH.format(6), 'r') as f:
            json_doc = json.load(f)
            pack = pysenml.Pack.from_json(json_doc)

            assert id(pack.measurements[0].base) == id(pack.measurements[1].base)
            assert id(pack.measurements[0].base) != id(pack.measurements[2].base)
            assert id(pack.measurements[2].base) == id(pack.measurements[3].base)

    def test_loads_all_fixtures_no_errors(self):
        for i in range(1, 14):
            filename = FIXTURES_PATH.format(i)
            with open(filename, 'r') as f:
                json_doc = json.load(f)
                pysenml.Pack.from_json(json_doc)

    def test_resolution(self):
        with open(FIXTURES_PATH.format(2), 'r') as f:
            json_doc = json.load(f)
            pack = pysenml.Pack.from_json(json_doc)
            pack.resolve()
            assert pack.measurements[0].name == 'urn:dev:ow:10e2073a01080063:voltage'
